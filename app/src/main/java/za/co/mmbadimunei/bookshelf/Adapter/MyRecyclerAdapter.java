package za.co.mmbadimunei.bookshelf.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import za.co.mmbadimunei.bookshelf.Model.BookModel;
import za.co.mmbadimunei.bookshelf.R;

/**
 * Created by Admin-123 on 2017-03-17.
 */
public class MyRecyclerAdapter extends  RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {

    private ArrayList<BookModel> androidModelArrayList = new ArrayList<>();
    DisplayImageOptions options;
    ImageLoader imageLoader;
    private OnItemClickListener mListener;
 Context context;
    int viewBooksBy =0;
    public class MyViewHolder extends RecyclerView.ViewHolder{

        public de.hdodenhof.circleimageview.CircleImageView imageViewThumb;
        public TextView textViewName;
        public MyViewHolder(View itemView) {
            super(itemView);
            //for making image circle shape
            imageViewThumb = (de.hdodenhof.circleimageview.CircleImageView)itemView.findViewById(R.id.list_image);
            textViewName = (TextView)itemView.findViewById(R.id.list_text);

        }
    }

    //handle clicking an item on a recyclerview

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }

    public void SetOnItemClickListener(OnItemClickListener mItemClickListener) {

        this. mListener = mItemClickListener;

    }

    public MyRecyclerAdapter(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .build();
this.context = context;
        //using Universal Image loader to download images online
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.img_not_available)
                .showImageOnFail(R.drawable.img_not_available)
                .delayBeforeLoading(1000)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    @Override
    public MyRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rowlayout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyRecyclerAdapter.MyViewHolder holder, final int position) {

      BookModel item = androidModelArrayList.get(position);
        if(item!=null) {

           imageLoader.displayImage(item.getBookImageUrl().toString(),holder.imageViewThumb, options, null);
//check which number is passed then set the TextView with needed data and color
            if(viewBooksBy==0) {
                holder.textViewName.setText(item.getBookTitle());
                holder.textViewName.setTextColor(context.getResources().getColor( R.color.colorAccent));
            }else if(viewBooksBy==1){
                holder.textViewName.setText(item.getBookAuthor());
                holder.textViewName.setTextColor(context.getResources().getColor( R.color.Bleu_De_France));
            }
            else if(viewBooksBy==2){
                holder.textViewName.setText(item.getBookPublishedDate());
                holder.textViewName.setTextColor(context.getResources().getColor( R.color.Citrine));
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener!=null){
                        mListener.onItemClick(position);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return androidModelArrayList.size();
    }

    //method to add all data after it finish downloading
    public void AddData( ArrayList<BookModel> bookModels){
        androidModelArrayList = bookModels;
        notifyDataSetChanged();
    }
     public void viewBy(int number){
    viewBooksBy =number;
     notifyDataSetChanged();
      }

}
