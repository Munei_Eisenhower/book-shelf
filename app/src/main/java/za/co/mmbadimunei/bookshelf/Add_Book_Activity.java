package za.co.mmbadimunei.bookshelf;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import za.co.mmbadimunei.bookshelf.Connection.JSONParser;
import za.co.mmbadimunei.bookshelf.Model.BookModel;

public class Add_Book_Activity extends AppCompatActivity {

    DisplayImageOptions options;
    ImageLoader imageLoader;
    ImageView imageViewCover;
    EditText editTextTitle,editTextDes,editTextAuthor,editTextPub;

    Calendar myCalendar;

    public ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__book);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Book");

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.setIndeterminate(true);

        editTextAuthor = (EditText)findViewById(R.id.et_author);
        editTextTitle = (EditText)findViewById(R.id.et_title);
        editTextDes = (EditText)findViewById(R.id.et_description);
        editTextPub = (EditText)findViewById(R.id.et_pub);
        imageViewCover = (ImageView) findViewById(R.id.imageCover);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .build();

        //using Universal Image loader to download images online
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.img_not_available)
                .showImageOnFail(R.drawable.img_not_available)
                .delayBeforeLoading(1000)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        myCalendar = Calendar.getInstance();



        editTextPub.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDateDialog();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
if(validate())
        new AddBook().execute(getString(R.string.server_url));

            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //preset the image cover from the url and display it to user
        //this made me not to allow user to add their own picture
        imageLoader.displayImage(getString(R.string.default_image_cover), imageViewCover, options, null);
    }

    public BookModel getUserData(){
        BookModel bookModel;
        String  title = editTextTitle.getText().toString();
        String  des = editTextDes.getText().toString();
        String  author = editTextAuthor.getText().toString();
        String  pub = editTextPub.getText().toString();
        bookModel = new BookModel(title,pub,des,author,getString(R.string.default_image_cover));

        return bookModel;
    }

    public boolean validate(){
        boolean valid=true;
        if(editTextTitle.getText().length()<1){
            editTextTitle.setError("Add a Title");
            valid=false;
        }
        if(editTextAuthor.getText().length()<1){
            editTextAuthor.setError("Add Author");
            valid =false;
        }
        return valid;
    }

    private class AddBook extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONParser jsonParser = new JSONParser();
            return jsonParser.POST(params[0],getUserData());
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String message ="Something went wrong! try again.";
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();

            }
if(!s.equals("failed")){
  try {
        JSONObject jsonObject = new JSONObject(s);
        Log.i("Results123", s);
        Log.v("11111", "qqqqqqqqqqqqqqqqqq  dsfdsf");
        if(jsonObject.has("id")) {
            // when you add successfully clear the EditText in case there is more books to be added
            clearUI();
            message ="Book Added:"+jsonObject.optString("title");
        }
    }catch (JSONException e){
        e.printStackTrace();
    }
}
      Snackbar.make(getCurrentFocus(),message , Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }
    }

    public void clearUI(){

        editTextTitle.setText("");
        editTextAuthor.setText("");
        editTextDes.setText("");
        editTextPub.setText("");
        imageLoader.displayImage(getString(R.string.default_image_cover), imageViewCover, options, null);
    }

    //use datepicker to make it easy to use and avoid unwanted input
   public  void showDateDialog(){
  DatePickerDialog datePicker =  new DatePickerDialog(Add_Book_Activity.this, date, myCalendar
            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH));
       //to avoid selecting future date to be selected
     datePicker.getDatePicker().setMaxDate(new Date().getTime());
     datePicker.show();
}

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateUI();
        }

    };
    private void updateUI() {
        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        editTextPub.setText(sdf.format(myCalendar.getTime()));
    }
}