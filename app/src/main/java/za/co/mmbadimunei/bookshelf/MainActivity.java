package za.co.mmbadimunei.bookshelf;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;

import za.co.mmbadimunei.bookshelf.Adapter.MyRecyclerAdapter;
import za.co.mmbadimunei.bookshelf.Connection.JSONParser;
import za.co.mmbadimunei.bookshelf.Connection.MyNetwork;
import za.co.mmbadimunei.bookshelf.Model.BookModel;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MyRecyclerAdapter.OnItemClickListener{

    Snackbar snackbar=null;
    private boolean isReceiverRegistered = false;
    RecyclerView myRecyclerView;
    MyRecyclerAdapter myRecyclerAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    NetworkChangeReceiver networkChangeReceiver;
    ArrayList<BookModel> androidModelArrayList = new ArrayList<>();
    public ProgressDialog mProgressDialog;
    JSONArray jsonArrayAll  = new JSONArray();
    private String nextTokenPage="";
    boolean isStart=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//Configuration of Image Universal Loader
        if(!ImageLoader.getInstance().isInited()) {
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                    .build();
            ImageLoader.getInstance().init(config);
        }

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.setIndeterminate(true);


        networkChangeReceiver = new NetworkChangeReceiver();

        myRecyclerView = (RecyclerView)findViewById(R.id.my_recycler_view);
        myRecyclerAdapter = new MyRecyclerAdapter(this);
        myRecyclerAdapter.SetOnItemClickListener(this);
        mLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLayoutManager);
        myRecyclerView.setAdapter(myRecyclerAdapter);

        //Using Network state to determine if it should display the sneakBar or load data
        ShowSneakBar(MyNetwork.isNetworkAvailable(this));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
if(MyNetwork.isNetworkAvailable(this)) {
    if (id == R.id.nav_add) {
        startActivity(new Intent(MainActivity.this, Add_Book_Activity.class));
    }
    //Change the title on the toolbar and assign a value to Adapter to know which data to dispaly
    else if(id==R.id.nav_view_by_author){
    myRecyclerAdapter.viewBy(1);
        getSupportActionBar().setTitle("Book Shelf - by Authors");
    }
    else if(id==R.id.nav_pub){
        myRecyclerAdapter.viewBy(2);
        getSupportActionBar().setTitle("Book Shelf - by Published Date");
    }
    else if(id==R.id.nav_All){
        myRecyclerAdapter.viewBy(0);
        getSupportActionBar().setTitle("Book Shelf");
    }
}
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    //Background process to download the JSON file and add data to RecyclerView with a string as parameter and JSON as a results
    private class GetBook extends AsyncTask<String,Void,JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
         mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONParser jsonParser = new JSONParser();
            return jsonParser.getJSONFromUrl(params[0]);

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            if(jsonObject!=null) {

               JSONArray jsonArray;

               try {
                   jsonArray = jsonObject.getJSONArray("items");

                   for(int a=0;a<jsonArray.length();a++)
                        jsonArrayAll.put(jsonArray.get(a));

                   BookModel bookModel;
                   for (int count = 0; count < jsonArray.length(); count++) {
                      JSONObject jsonObjectAndroid = jsonArray.getJSONObject(count);

                        //Set JSON directly to Object
                     bookModel = new BookModel(jsonObjectAndroid);
                       //add all books on the page to the array
                     androidModelArrayList.add(bookModel);
                }

                   //Check if the key nextPageToken is a string or a boolean
                   //if it is a string it will read the value and encode the url
                   if(jsonObject.get("nextPageToken") instanceof String  ){
                       if(!nextTokenPage.equals(jsonObject.getString("nextPageToken"))) {
                           try {
                               nextTokenPage = jsonObject.optString("nextPageToken");

                               String afterDecode = Uri.encode(nextTokenPage);

                              //to avoid it loading many times
                               if(myRecyclerAdapter.getItemCount()==0)
                               new GetBook().execute(getString(R.string.server_url) + "?pageToken=" + afterDecode);
                           }catch(Exception e){
                               e.printStackTrace();
                           }
                       }
                   }

                    //add arraylist of the data to recyclerview adapter
                 myRecyclerAdapter.AddData(androidModelArrayList);

                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }
    public class NetworkChangeReceiver extends BroadcastReceiver {
        public NetworkChangeReceiver() {
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {

            boolean status = MyNetwork.isNetworkAvailable(context);

//if any change call this method
          ShowSneakBar(status);


        }

    }

    protected void onResume() {
        super.onResume();
        //register BroadcastReceiver listener
       if (!isReceiverRegistered) {
            isReceiverRegistered = true;

          registerReceiver(networkChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")); // IntentFilter to wifi state change is "android.net.conn.CONNECTIVITY_CHANGE"
        }
    }
    protected void onPause() {
        super.onPause();
        //unregister  BroadcastReceiver listener
       if (isReceiverRegistered) {
          isReceiverRegistered = false;

            unregisterReceiver( networkChangeReceiver);
      }
    }

    @Override
    public void onItemClick(int position) {
        if(MyNetwork.isNetworkAvailable(this)) {
            JSONObject jsonObjectSelected = null;
            try {
                jsonObjectSelected = jsonArrayAll.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //send data from this activity to the next activity using json string
            if (jsonObjectSelected != null) {
                Intent intent = new Intent(MainActivity.this, BookDetails.class);
                intent.putExtra("myBookData", jsonObjectSelected.toString());
                startActivity(intent);
            }
        }
    }
    public void ShowSneakBar(boolean isConnected){
//if it is connected to the network close snackbar
        if(isConnected) {
            if(snackbar!=null) {
                snackbar.dismiss();
            }
            //if it is connected to the network and the RecyclerAdapter is empty download data and set
            //to prevent it to download every time an activity resume
            if(myRecyclerAdapter.getItemCount()==0 && isStart) {
                isStart=false;
                new GetBook().execute(getString(R.string.server_url));

            }

        }else {
            //if it is not connected and was busy loading dismiss ProgressDialog
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }

//Write a red message on snackbar to show not connected
            String message;
            int color;

            message = "Sorry! Not connected to internet";
            color = Color.RED;
            View parentLayout = findViewById(R.id.root_layer);

            snackbar = Snackbar
                    .make(parentLayout, message, Snackbar.LENGTH_INDEFINITE);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }
}
