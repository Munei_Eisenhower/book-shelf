package za.co.mmbadimunei.bookshelf;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import za.co.mmbadimunei.bookshelf.Connection.JSONParser;
import za.co.mmbadimunei.bookshelf.Model.BookModel;

/**
 * Created by Admin-123 on 2017-03-18.
 */
public class BookDetails extends AppCompatActivity {
    TextView textViewTitle, textViewAuthor, textViewDescription, textViewPublishDate;
    ImageView imageViewCover;
    String jsonString = "";
    DisplayImageOptions options;
    ImageLoader imageLoader;
    JSONObject jsonObject = new JSONObject();
    String title, description, author, publishedDate, imageUrl, id;
    JSONParser jsonParser = new JSONParser();
    BookModel bookModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);


        //to make a back arrow in this activity
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.img_not_available)
                .showImageOnFail(R.drawable.img_not_available)
                .delayBeforeLoading(1000)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        imageLoader = ImageLoader.getInstance();
        imageViewCover = (ImageView) findViewById(R.id.image_large);
        textViewTitle = (TextView) findViewById(R.id.tv_des);
        textViewDescription = (TextView) findViewById(R.id.tv_author);
        textViewAuthor = (TextView) findViewById(R.id.tv_pubDate);


//get the intent and extras passed from the last activity
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            jsonString = bundle.getString("myBookData");


            try {
                jsonObject = new JSONObject(jsonString);
                bookModel = new BookModel(jsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (bookModel != null) {
            getSupportActionBar().setTitle(bookModel.getBookTitle());
            textViewTitle.setText(bookModel.getBookDescription());
            textViewDescription.setText(bookModel.getBookAuthor());
            textViewAuthor.setText(bookModel.getBookPublishedDate());

            imageLoader.displayImage(bookModel.getBookImageUrl(), imageViewCover, options, null);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        //listen for that back arrow clicked and perform a backPressed
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (itemId == R.id.action_delete) {
new deleteBook().execute(getString(R.string.server_url) + bookModel.getBookId());

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private class deleteBook extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            jsonParser.Delete(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //if book is deleted we will like to get a fresh data so, just download a new list by reopening the activity instead of back
            startActivity(new Intent(BookDetails.this,MainActivity.class));
        }
    }
}