package za.co.mmbadimunei.bookshelf.Model;

import org.json.JSONObject;

/**
 * Created by Admin-123 on 2017-03-17.
 */
public class BookModel {
    String bookTitle;
    String bookDescription;
    String bookPublishedDate;
    String bookAuthor;
    String bookImageUrl;
    String bookId;

    public BookModel(String bookTitle, String bookPublishedDate, String bookDescription, String bookAuthor, String bookImageUrl) {
        this.bookTitle = bookTitle;
        this.bookPublishedDate = bookPublishedDate;
        this.bookDescription = bookDescription;
        this.bookAuthor = bookAuthor;
        this.bookImageUrl = bookImageUrl;
    }

    //Read a Json file
    public  BookModel(JSONObject jsonObjectAndroid){
        try {

            //check if the JSON item has an image field then set an empty string to help Universal Image Loader not to look for null address
            if(jsonObjectAndroid.has("imageUrl")) {
                this.bookImageUrl =  jsonObjectAndroid.optString("imageUrl");
            }else{
                this.bookImageUrl = "";
            }

            //use optString so it can return null if the field is not there

            //since you can post an empty string to add a book this will help to display some text in case it returns an empty string
            if(jsonObjectAndroid.optString("title").length()<1){
                this.bookTitle ="Missing Title";
            }else {
                this.bookTitle = jsonObjectAndroid.optString("title");
            }
            if(jsonObjectAndroid.optString("description").length()<1){
                this.bookDescription = "No Description";
            }else {
                this.bookDescription = jsonObjectAndroid.optString("description");
            }
            if(jsonObjectAndroid.optString("author").length()<1){
                this.bookAuthor ="Unknown Author";
            }else {
                this.bookAuthor = jsonObjectAndroid.optString("author");
            }
            if(jsonObjectAndroid.optString("publishedDate").length()<1){
                this.bookPublishedDate ="Unknown Date";
            }else {
                this.bookPublishedDate = jsonObjectAndroid.optString("publishedDate");
            }
            this.bookId = jsonObjectAndroid.optString("id");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public void setBookDescription(String bookDescription) {
        this.bookDescription = bookDescription;
    }

    public String getBookPublishedDate() {
        return bookPublishedDate;
    }

    public void setBookPublishedDate(String bookPublishedDate) {
        this.bookPublishedDate = bookPublishedDate;
    }

    public String getBookImageUrl() {
        return bookImageUrl;
    }

    public void setBookImageUrl(String bookImageUrl) {
        this.bookImageUrl = bookImageUrl;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }
}
